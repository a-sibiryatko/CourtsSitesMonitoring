class Observable:
    def __init__(self):
        self.observers = []

    def register(self, o):
        self.observers.append(o)

    def un_register(self, o):
        self.observers.remove(o)

    def notify(self, event, arg):
        for o in self.observers:
            o.update(self, event, arg)
