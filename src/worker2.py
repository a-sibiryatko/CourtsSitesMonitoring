import threading

__author__ = 'Various'


class Worker:

    def __init__(self):
        self.event = threading.Event()

    def work(self, data):
        pass

    def stop(self):
        self.event.set()
