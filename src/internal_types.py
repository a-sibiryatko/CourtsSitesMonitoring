import functools
from enum import Enum, IntEnum


# # Класс для судов:
class Court:
    def __init__(self, court_type, court_name, court_address):
        self.cname = court_name
        self.address = court_address
        self.type = court_type

    def __call__(self):
        return self.type, self.cname, self.address

    def __str__(self):
        return "Тип суда - {0}. Суд - {1}.\nАдрес суда - {2}.\n".format(self.type,
                                                                        self.cname,
                                                                        self.address)

    def __add__(self, value):
        return str(self.address) + str(value)

    def get_court_type(self):
        return self.type

    def get_court_name(self):
        return self.cname

    def get_court_address(self):
        return self.address


# # Класс для дат
@functools.total_ordering
class Date:
    def __init__(self, year, start, end):
        self.year = year
        self.start = start
        self.end = end

    def __str__(self):
        return (
            "Год - {0}. Начало периода - {1}. Конец периода - {2}.".format(self.year,
                                                                           self.start,
                                                                           self.end))

    def __eq__(self, other):
        return self.year == other.year

    def __gt__(self, other):
        return self.year > other.year

    def get_year(self):
        return self.year

    def get_start(self):
        return self.start

    def get_end(self):
        return self.end


# # Singleton глобальной остановки
class GlobalStop:
    sign = False


class CourtType(Enum):
    LevelOne = 'Суды 1-ой инстанции'
    LevelTwo = 'Суды 2-ой инстанции'

    @staticmethod
    def list():
        return list(e.value for e in CourtType)

    def text(self):
        return self.value


class CaseTypeL1(Enum):
    ALL_TYPES = {'text': 'Все дела', 'calc_range': [i for i in range(14)], 'browse_range': [i for i in range(10)]}
    G1_CASE = {'text': 'Гражд. 1-я инстанция', 'calc_range': [0, 1, 2, 3, 4, 5], 'browse_range': [0, 1, 2, 3]}
    U1_CASE = {'text': 'Угол. 1-я инстанция', 'calc_range': [6, 7], 'browse_range': [6, 7]}
    ADM1_CASE = {'text': 'Дела об АП 1-я инстанция', 'calc_range': [8, 9, 10, 11, 12, 13], 'browse_range': [8, 9]}

    @staticmethod
    def list():
        return list(e.text() for e in CaseTypeL1)

    @staticmethod
    def from_string(name):
        for elem in CaseTypeL1:
            if elem.value['text'] == name:
                return elem

    def text(self):
        return self.value['text']

    def browse_range(self):
        return self.value['browse_range']

    def calc_range(self):
        return self.value['calc_range']


class CaseTypeL2(Enum):
    ALL_TYPES = {'text': 'Все дела', 'calc_range': [i for i in range(14)], 'browse_range': [i for i in range(10)]}
    G1_CASE = {'text': 'Гражд. 1-я инстанция', 'calc_range': [0, 5], 'browse_range': [0, 1, 2, 3]}
    U1_CASE = {'text': 'Угол. 1-я инстанция', 'calc_range': [6, 7], 'browse_range': [6, 7]}
    ADM1_CASE = {'text': 'Дела об АП 1-я инстанция', 'calc_range': [8, 9, 10, 11, 12, 13], 'browse_range': [8, 9]}

    @staticmethod
    def list():
        return list(e.text() for e in CaseTypeL1)

    @staticmethod
    def from_string(name):
        for elem in CaseTypeL1:
            if elem.value['text'] == name:
                return elem

    def text(self):
        return self.value['text']

    def browse_range(self):
        return self.value['browse_range']

    def calc_range(self):
        return self.value['calc_range']


class Year(IntEnum):
    first_year = 0
    second_year = 1
    third_year = 2


class Period(IntEnum):
    first_period = 0
    second_period = 1

if __name__ == '__main__':
    # print(TypesNew.ADM_CASE_)
    print(CaseTypeL1.ALL_TYPES.browse_range())
    for el in CaseTypeL1.list():
        print(el)

    print(CaseTypeL1.ALL_TYPES.calc_range())

    print('CourtTypeOne = ', CourtType.LevelOne.text())
    for el in CourtType.list():
        print(el)