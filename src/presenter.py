import os
import threading
import webbrowser
import subprocess
import time
from src import analyzing
from src.analyzing_worker import AnalyzingWorker
from src.observer import Observer
from src.internal_types import Court, Date, CourtType, CaseTypeL1, Year, GlobalStop
from multiprocessing import Pool, Value
from tkinter import messagebox

__author__ = 'Various'

class Presenter(Observer):
    def __init__(self, model):
        self.model = model
        self.view = None
        self.workers = []

    def analyze(self):
        if not self.folder_option():
            messagebox.showerror(title="Ошибка сохранения", message="Открыт файл отчета, либо у Вас нет прав записи данных в каталог отчетов.")
            return

        self.view.change_state('DISABLED')

        pool = Pool(1)
        for year in self.view.checked_years:
            data = {}
            data['court_type'] = self.view.current_court_type
            data['courts'] = self.view.current_courts
            data['first_date'] = self.view.date_fields[year * 2].get()
            data['second_date'] = self.view.date_fields[year * 2 + 1].get()
            data['year'] = self.view.years_index[year]
            data['case_type'] = self.view.current_case_type
            data['judge'] = self.view.judge

            # analyzing.checkSites(
            #                      self.view.current_court_type,
            #                      self.view.current_courts,
            #                      self.view.date_fields[year * 2].get(),
            #                      self.view.date_fields[year * 2 + 1].get(),
            #                      self.view.years_index[year],
            #                      self.view.current_case_type,
            #                      self.view.judge)
            worker = AnalyzingWorker()
            self.workers.append(worker)
            worker.work(data)

        self.workers = []
        self.view.change_state('ENABLED')

    def folder_option(self, and_open=False):
        try:
            subprocess.Popen('if not exist "%s" mkdir "%s"' % (os.environ.get("APPDATA") + "\\Мониторинг\\",
                                                               os.environ.get("APPDATA") + "\\Мониторинг\\"),
                             shell=True)
            if and_open:
                subprocess.Popen('if exist "%s" explorer "%s"' % (os.environ.get("APPDATA") + "\\Мониторинг\\",
                                                          os.environ.get("APPDATA") + "\\Мониторинг\\"), shell=True)
        except OSError:
            return False

        return True

    def open_browser(self, year):
        if self.view.court_type_field.get() == CourtType.LevelOne.text():
                url_list = analyzing.make_url(self.view.date_fields[year * 2].get(),
                                              self.view.date_fields[year * 2 + 1].get(),
                                              CourtType.LevelOne,
                                              self.view.judge)

                # �������� ��������� ���
                court = self.model.get_courts_by_type(CourtType.LevelOne)[self.view.court.current()]

                case_type = CaseTypeL1.from_string(self.view.case_type.get())
                final_url = [court.get_court_address() + url_list[i] for i in case_type.browse_range()]

                for i in range(len(final_url)):
                    threading.Timer(1.25, webbrowser.open, args=(final_url[i],)).start()

        elif self.view.court_type_field.get() == CourtType.LevelTwo.text():
            # ����� ������, ���� ������ ��� ���
            pass
        else:
            pass

    def set_view(self, view):
        self.view = view
        self.model.register(view)

    def get_years(self):
        return self.model.get_years()

    def get_all_periods(self):
        return self.model.get_all_periods()

    def get_courts_by_level(self, court_type):
        return self.model.get_courts_by_type(court_type)

    def update(self, event, arg):
        pass

    def stop(self):
        for worker in self.workers:
            worker.stop()