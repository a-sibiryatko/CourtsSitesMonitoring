__author__ = 'Various'
import os
import re
import tkinter.messagebox as mb

import src.loader as loader
from src.internal_types import Date, Court, CourtType
from src.observable2 import Observable


class Model(Observable):
    def __init__(self):
        Observable.__init__(self)
        self.court_types = []
        self.judges = []
        self.dates = []
        self.level1_courts, self.level2_courts = [], []
        self.loaded_data = {}

        data = loader.load_data()
        courts = data[0]
        self.dates = data[1]
        self.sort_courts_by_type(courts)

    def make(self):
        pass

    def sort_courts_by_type(self, courts):
        # courts = court_tuple[0]
        for i in range(len(courts)):  # courts = массив объектов типа Court
            if courts[i].get_court_type() in ['РС', 'ГВС']:
                self.level1_courts.append(courts[i])

            elif courts[i].get_court_type() in ['ОС', 'ОВС']:
                self.level2_courts.append(courts[i])

        empty_court = Court("Все уровни", "Все суды", "")
        self.level1_courts.insert(0, empty_court)
        self.level2_courts.insert(0, empty_court)

    def get_courts_by_type(self, court_type):
        if court_type == CourtType.LevelOne:
            return self.level1_courts
        elif court_type == CourtType.LevelTwo:
            return self.level2_courts
        else:
            return []

    def get_years(self):
        return sorted(date.get_year() for date in self.dates)

    def get_all_periods(self):
        periods = []
        for date in sorted(self.dates):
            periods.append(date.get_start())
            periods.append(date.get_end())

        return periods

if __name__ == '__main__':
    model = Model()
    print(model.get_courts_by_type(CourtType.LevelOne))
    print(model.get_years())
    print(model.dates)
    print(sorted(model.dates))