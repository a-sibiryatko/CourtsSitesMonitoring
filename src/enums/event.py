from enum import Enum

__author__ = 'Various'

class Event(Enum):
    FINISHED = 'FINISHED'
    RESULT = 'RESULT'
    COMMON_ERROR = 'COMMON_ERROR'
    FILE_ERROR = 'FILE_ERROR'
    INTERNET_ERROR = 'INTERNET_ERROR'