import subprocess
import os
import datetime
from threading import Thread
import threading
from tkinter import *
import tkinter
import tkinter.ttk as ttk
import tkinter.messagebox as mb

from src.internal_types import GlobalStop, Court, Year, Period, CourtType, CaseTypeL1, CaseTypeL2
from src.observer import Observer
from src.presenter import Presenter
import src.calendar_GUI as calendar_GUI
import src.analyzing as analyzing
from src.model import Model

__author__ = 'Various'

# # Перечень типов судопроизводства
level1_case_types = ["Все дела", "Гражд. 1-я инстанция", "Угол. 1-я инстанция", "Дела об АП 1-я инстанция"]

# # Перечень типов судопроизводства и список судов для ОВС
level2_case_types = ["Все дела", "Градж. 1-я инстанция", "Угол. 1-я инстанция", "Градж. апелляция", "Угол. апелляция",
                     "Гражд. кассация", "Угол. кассация", "Надзор по делам об АП"]


# # Основное окно программы
class MainWindow(Toplevel, Observer):
    # def __init__(self):
    def __init__(self, presenter, master):
        super().__init__(master)
        self.title("Мониторинг сайтов судов")
        self.geometry("890x700+400+100")
        self.resizable(width=FALSE, height=FALSE)

        self.check_button_vars = None
        # self.root = master
        self.presenter = presenter

        self.all_court_types = CourtType.list()
        self.current_courts = []
        self.current_case_type = None
        self.current_court_type = None
        self.checked_years = []
        self.level1_court_types = ["РС", "ГВС"]
        self.level2_court_types = ["ОС", "ОВС"]

        self.level1_courts = self.presenter.get_courts_by_level(CourtType.LevelOne)
        self.level2_courts = self.presenter.get_courts_by_level(CourtType.LevelTwo)
        self.periods = self.presenter.get_all_periods()
        self.date_fields = []
        self.calendar_fields = []
        self.check_button_vars = []
        self.years_index = self.presenter.get_years()
        self.judge = ''

        self.level1_combobox_list = [self.level1_courts[0].get_court_name()] + [
            court.get_court_name() + ', ' + court.get_court_type()
            for court in self.level1_courts[1:]]
        self.level2_combobox_list = [self.level2_courts[0].get_court_name()] + [
            court.get_court_name() + ', ' + court.get_court_type()
            for court in self.level2_courts[1:]]

        styleBut = ttk.Style()
        styleBut.configure("TButton", font=("Times New Roman", 12), background="white smoke")
        styleBut2 = ttk.Style()
        styleBut2.configure("C.TButton", width=2, font=("Times New Roman", 12), fieldbackground="black",
                            background="black", foreground="red")
        style_but3 = ttk.Style()
        style_but3.configure("S.TButton", font=("Times New Roman", 12, "bold"), foreground="red")

        style_progress_bar = ttk.Style()
        style_progress_bar.configure("Horizontal.TProgressbar", background="white")

        styleCBox = ttk.Style()
        styleCBox.configure("TCombobox", font=("Times New Roman", 20))

        styleLabel = ttk.Style()
        styleLabel.configure("TLabel", font=("Times New Roman", 12, "bold"), bordercolor="black")

        styleCBut = ttk.Style()
        styleCBut.configure("TCheckbutton", font=("Times New Roman", 14), background="snow2")

        self.left_top_frame = Frame(self, bg="snow2")
        self.left_bottom_frame = Frame(self, bg="snow2")
        self.right_frame = Frame(self, bg="snow2")

        self.left_top_frame.place(x=0, y=0, width=700, height=400)
        self.left_bottom_frame.place(x=0, y=400, width=700, height=300)
        self.right_frame.place(x=700, y=0, width=190, height=700)

        self.if_calendar_exist = False

        self._state = 'ENABLED'

        self.label_court_type = ttk.Label(self.left_top_frame, text="Тип суда", style="TLabel", anchor=CENTER,
                                          background="snow2")
        self.label_court_type.place(x=0, y=5, width=180, height=30)
        self.court_type_field = ttk.Combobox(self.left_top_frame, values=self.all_court_types, state="readonly",
                                             style="TCombobox")
        self.court_type_field.current(0)
        self.court_type_field.bind("<<ComboboxSelected>>", self._on_court_type_focus_out)
        self.court_type_field.place(x=20, y=35, width=180, height=35)
        self.label_case = ttk.Label(self.left_top_frame, text="Вид судопр-ва", style="TLabel", anchor=CENTER,
                                    background="snow2")
        self.label_case.place(x=200, y=5, width=180, height=30)
        self.case_type = ttk.Combobox(self.left_top_frame, values=CaseTypeL1.list(), state="readonly",
                                      style="TCombobox")
        self.case_type.current(0)
        self.case_type.bind("<<ComboboxSelected>>", self._on_court_focus_out)
        self.case_type.place(x=220, y=35, width=190, height=35)
        self.label_court = ttk.Label(self.left_top_frame, text="Наим. суда", style="TLabel", anchor=CENTER,
                                     background="snow2")
        self.label_court.place(x=420, y=5, width=180, height=30)
        self.court = ttk.Combobox(self.left_top_frame, values=self.level1_combobox_list, state="readonly",
                                  style="TCombobox")
        self.court.current(0)
        self.court.bind("<<ComboboxSelected>>", self._on_court_focus_out)
        self.court.place(x=430, y=35, width=180, height=35)
        self.judge_case_label = ttk.Label(self.left_top_frame, text="Судья", style="TLabel", anchor=CENTER,
                                          background="snow2")
        self.judge_case_label.place(x=420, y=70, width=180, height=30)
        self.judge_list = ["Все судьи"]
        self.judge = self.judge_list[0]
        self.judge_case = ttk.Combobox(self.left_top_frame, values=self.judge_list, state=DISABLED, style="TCombobox")
        self.judge_case.current(0)
        self.judge_case.bind('<<ComboboxSelected>>', self._on_judge_case_selected)
        self.judge_case.place(x=430, y=100, width=180, height=35)

        geometry = {}
        geometry['date_field_width'] = 120
        geometry['date_field_height'] = 35
        geometry['date_field_x'] = 20
        geometry['date_field_x_step'] = geometry['date_field_width'] + 60
        geometry['date_field_y'] = 160
        geometry['date_field_y_step'] = 90
        geometry['calendar_height'] = 35
        geometry['calendar_x'] = geometry['date_field_x'] + geometry['date_field_width']
        geometry['calendar_y'] = geometry['date_field_y']
        geometry['calendar_x_step'] = geometry['date_field_x_step']
        geometry['calendar_y_step'] = geometry['date_field_y_step']
        geometry['cb_width'] = 120
        geometry['cb_x'] = 20
        geometry['cb_y'] = 120
        geometry['cb_y_step'] = 90

        self.onKeyEntry = self.register(self.onKeyEntry_registered)

        self._calendar_selection = None

        for year in Year:
            check_button_var = IntVar()
            check_button_var.set(0)
            check_button_year = ttk.Checkbutton(self.left_top_frame, text=self.years_index[year],
                                                variable=check_button_var, style="TCheckbutton")
            check_button_year.place(x=geometry['cb_x'],
                                    y=geometry['cb_y'] + geometry['cb_y_step'] * year,
                                    width=geometry['cb_width'])
            self.check_button_vars.append(check_button_var)

            for period in Period:
                date_field = Entry(self.left_top_frame, font=("times", 14), fg="snow4", validate="key",
                                   validatecommand=self.onKeyEntry + " %P")
                date_field.insert("0", self.periods[year * 2 + period])
                date_field.bind("<FocusIn>",
                                lambda _, year=year, period=period: self._date_delete_handler(year, period))
                date_field.bind("<FocusOut>",
                                lambda _, year=year, period=period: self._date_insert_handler(year, period))

                calendar_button = ttk.Button(self.left_top_frame, text="K", style="C.TButton",
                                             command=lambda year=year, period=period: self._calendar_button_handler(
                                                 year,
                                                 period))

                date_field.place(x=geometry['date_field_x'] + geometry['date_field_x_step'] * period,
                                 y=geometry['date_field_y'] + geometry['date_field_y_step'] * year,
                                 width=geometry['date_field_width'], height=geometry['date_field_height'])

                calendar_button.place(x=geometry['calendar_x'] + geometry['calendar_x_step'] * period,
                                      y=geometry['calendar_y'] + geometry['calendar_y_step'] * year,
                                      height=geometry['calendar_height'])

                self.date_fields.append(date_field)
                self.calendar_fields.append(calendar_button)

        self.browser_button1 = ttk.Button(self.left_top_frame, text="Открыть в браузере", style="TButton",
                                          command=lambda: self.open_in_browser(Year.first_year))

        self.browser_button2 = ttk.Button(self.left_top_frame, text="Открыть в браузере", style="TButton",
                                          command=lambda: self.open_in_browser(Year.second_year))

        self.browser_button3 = ttk.Button(self.left_top_frame, text="Открыть в браузере", style="TButton",
                                          command=lambda: self.open_in_browser(Year.third_year))

        self.browser_button1.place(x=380, y=160, width=170)
        self.browser_button2.place(x=380, y=250, width=170)
        self.browser_button3.place(x=380, y=340, width=170)

        bottom_separator = ttk.Separator(self.left_bottom_frame, orient="horizontal")
        bottom_separator.pack(side="top", fill="x")
        self.main_text = Text(self.left_bottom_frame, font=("times", 12), wrap=CHAR, state=DISABLED)
        self.yscroll = ttk.Scrollbar(self.left_bottom_frame, orient="vertical", command=self.main_text.yview)
        self.main_text.configure(yscrollcommand=self.yscroll.set)
        self.main_text.place(x=20, y=10, width=650, height=180)
        self.yscroll.place(x=670, y=10, width=20, height=180)
        self.auto_scroll_var = IntVar()
        self.auto_scroll_var.set(0)
        self.auto_scroll_cbutton = ttk.Checkbutton(self.left_bottom_frame, text="Отключить автопрокрутку текста",
                                                   variable=self.auto_scroll_var)
        self.auto_scroll_cbutton.place(x=20, y=200)
        self.PB = ttk.Progressbar(self.left_bottom_frame, orient="horizontal", mode="determinate", maximum=16,
                                  style='Horizontal.TProgressbar')
        self.PB.place(x=20, y=245, width=670, height=30)

        left_separator = ttk.Separator(self.right_frame, orient="vertical")
        left_separator.pack(side="left", fill="y")

        self.analyze_button = ttk.Button(self.right_frame, text="Проверка", style="TButton")
        self.analyze_button.place(x=10, y=30, width=170)
        self.analyze_button.bind("<Button-1>", self.analyze)

        self._stop_button = ttk.Button(self.right_frame, text="СТОП", style="S.TButton")
        self._stop_button.bind("<Button-1>", self._stop_button_handler)

        self.open_folder_button = ttk.Button(self.right_frame, text="   Открыть\nпапку отчетов", style="TButton")
        self.open_folder_button.place(x=10, y=70, width=170, height=60)
        self.open_folder_button.bind("<Button-1>", self.open_folder)

        right_top_separator = ttk.Separator(self.right_frame, orient="horizontal")
        right_top_separator.place(y=150, width=190)
        right_bottom_separator = ttk.Separator(self.right_frame, orient="horizontal")
        right_bottom_separator.place(y=520, width=190)

        self.about_button = ttk.Button(self.right_frame, text="О программе", style="TButton")
        self.about_button.place(x=10, y=540, width=170)
        self.about_button.bind("<Button-1>", self.about)

        self.quit_button = ttk.Button(self.right_frame, text="Закрыть", style="TButton")
        self.quit_button.place(x=10, y=580, width=170)
        self.quit_button.bind("<Button-1>", self.quit_h)

    # # Date field validator
    def onKeyEntry_registered(self, val):
        i = 0
        dateflag = True
        dateArr = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."]
        while (i < len(val)) and (dateflag):
            if (val[i] not in dateArr):
                dateflag = not dateflag
            i += 1
        if (dateflag) and (len(val) <= 10):
            return 1
        else:
            return 0

    # # Focus out handler for court_type
    def _on_court_type_focus_out(self, event):
        self.court.current(0)
        self.case_type.current(0)
        self.judge_case.current(0)
        self.judge_case.configure(state=DISABLED, values=['Все судьи'])

        if self.all_court_types[self.court_type_field.current()] == CourtType.LevelOne:
            self.court.configure(values=self.level1_combobox_list)
            self.case_type.configure(values=level1_case_types)

        elif self.all_court_types[self.court_type_field.current()] == CourtType.LevelTwo:
            self.court.configure(values=self.level2_combobox_list)
            self.case_type.configure(values=level2_case_types)

        self.label_court_type.focus_set()
        self.court.update()
        self.case_type.update()
        self.judge_case.update()

    # # Focus out handler for case_type, court
    def _on_court_focus_out(self, event):
        if self.court.current():
            self.judge_list = ["Все судьи"] + analyzing.load_judge_list(self.court_type_field.current(),
                                                                        self.case_type.current(),
                                                                        self.level1_courts[self.court.current()])
            self.judge_case.configure(state="readonly", values=self.judge_list)
            self.judge_case.current(0)
        else:
            self.judge_list = ['Все судьи']
            self.judge_case.configure(state=DISABLED, values=self.judge_list)
            self.judge_case.current(0)

    def _on_judge_case_selected(self, event):
        self.judge = self.judge_list[self.judge_case.current()] if self.judge_case.current() else None

    # # Focus out handler for date fields
    def _date_delete_handler(self, year, period):
        field = self.date_fields[year * 2 + period]

        field.configure(fg="black")
        if field.get() == self.periods[year * 2 + period]:
            field.delete("0", END)

    def _date_insert_handler(self, year, period):
        field = self.date_fields[year * 2 + period]
        if self._calendar_selection:
            field.configure(fg="black")
            field.delete("0", END)
            field.insert("0", self._calendar_selection)
        if field.get() == "":
            field.configure(fg="snow4")
            field.insert("0", self.periods[year * 2 + period])

    # # Autoscroll func
    def _scroll_to_end(self):
        if not self.auto_scroll_var.get():
            self.main_text.yview(END)

    # # Progressbar update func
    def _update_progress_bar(self):
        self.PB.step(1)

    # # Main text field update func
    def _update_main_text(self, Text):
        self.main_text.configure(state=NORMAL)
        self.main_text.insert(END, datetime.datetime.now().strftime("%H:%M:%S | ") + Text)

        if self.auto_scroll_var.get() == 0:
            self.main_text.yview(END)

        self.main_text.configure(state=DISABLED)
        self._scroll_to_end()

    ## Handler for Calendar buttons
    def _calendar_button_handler(self, year, period):
        if self.if_calendar_exist:
            del self.cal
            self.top.destroy()
        else:
            self.if_calendar_exist = not self.if_calendar_exist
        self.top = Toplevel()
        self.top.title("Календарь")
        self.cal = calendar_GUI.Calendar(self.top, master=self.top, )
        self.cal.pack(expand=1, fill='both')
        self.cal.bind('<Destroy>', lambda event: self._on_calendar_destroy(self, self.date_fields[year * 2 + period]))

    def _on_calendar_destroy(self, event, field):
        try:
            self._calendar_selection = self.cal.selection.strftime("%d.%m.%Y")
        except AttributeError:
            pass

        field.focus_set()
        self.label_court_type.focus_set()
        field.update()
        self._calendar_selection = None

    ## Disable fields func
    def _change_state(self):
        if self._state == 'DISABLED':
            self.court_type_field.configure(state=DISABLED)
            self.case_type.configure(state=DISABLED)
            self.court.configure(state=DISABLED)
            self.judge_case.configure(state=DISABLED)

            for field in self.date_fields:
                field.configure(state=DISABLED)

        elif self._state == 'ENABLED':
            self.court_type_field.configure(state="readonly")
            self.case_type.configure(state="readonly")
            self.court.configure(state="readonly")
            self.judge_case.configure(state="readonly")

            for field in self.date_fields:
                field.configure(state=NORMAL)

    def _accumulate_data(self):
        # Отбираем данные
        if self.court_type_field.current() == 0:  # выбрано "Уровень 1"
            if self.court.current() == 0:
                self.current_courts = self.level1_courts[1:]
            else:
                self.current_courts.append(self.level1_courts[self.court.current()])

            self.current_court_type = CourtType.LevelOne
            self.current_case_type = CaseTypeL1.from_string(self.case_type.get())

        elif self.court_type_field.current() == 1:
            self.current_courts = self.level2_courts[1:]

            self.current_court_type = CourtType.LevelTwo
            self.current_case_type = CaseTypeL2.from_string(self.case_type.get())

        else:
            return

        self.PB.configure(maximum=len(self.current_courts) * len(self.checked_years))
        self.checked_years = [year for year in Year if self.check_button_vars[year].get()]

    def change_state(self, state):
        # Изменяем состояние полей
        self._state = state
        self._change_state()

        ## Передаем фокус на кнопку и обновляем поля дат, чтобы они потеряли фокус
        if state == 'DISABLED':
            self.analyze_button.focus_set()
            for i in range(len(self.date_fields)):
                self.date_fields[i].update()

            # Убираем кнопку старта, размещаем кнопку остановки
            self.analyze_button.place_forget()
            self._stop_button.place(x=10, y=30, width=170)

        if state == 'ENABLED':
            self._stop_button.place_forget()
            self.analyze_button.place(x=10, y=30, width=170)
            self.PB.configure(maximum=16, value=0)

            # if not stop.sign:
            mb.showinfo(title="Сообщение", message="Проверка закончена!")
            # else:
            #     stop.sign = False
            #     self._update_main_text("Проверка прервана.\n")
            #     mb.showinfo(title="Сообщение", message="Проверка прервана.")

    ######################################################
    ## Buttons handlers

    ## Обработчик кнопки "Проверка"
    def analyze(self, event):
        self._accumulate_data()

        if not self.checked_years:
            self._scroll_to_end()
            self._update_main_text("Вы не выбрали год!\n")

        else:
            if not self.presenter.folder_option():
                return

            ## Если флаг не сброшен: все даты верны - приступаем к проверке судов
            if self._dates_check():
                for year in self.checked_years:
                    if not GlobalStop.sign:
                        self._update_main_text(
                                "Выбранный период в {0} году: с {1} по {2}.\nВыбранная инстанция - {3}.\n\n" \
                                .format(self.years_index[year],
                                        self.date_fields[year * 2].get(),
                                        self.date_fields[year * 2 + 1].get(),
                                        self.current_case_type.text()))

                thread = threading.Thread(target=self.presenter.analyze)
                thread.start()

    ## Обработчик кнопки "СТОП"
    def _stop_button_handler(self, event):
        self.presenter.stop()

    ## Функция проверки даты на соответствие
    def _dates_check(self, _year=None):
        date_check = True

        p = re.compile(
            "(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d")  # date format dd(-./)mm(-./)yyyy
        # Создаем список годов из чекпоинтов
        if _year is None:
            years = self.checked_years
        # Если год передан, значит мы вызываем ф-ию открытия браузера
        # и страниц определенной категории
        else:
            years = [_year]
        # Проверяем поля.  Если не соотств. - выдаем error и сбрасываем флаг
        for year in years:
            if p.match(self.date_fields[year * 2].get()) is None or p.match(
                    self.date_fields[year * 2 + 1].get()) is None:
                mb.showerror(title="Неверная дата!",
                             message="Дата выбрана неверно! Проверьте поля дат за {0} год.\nФормат даты: DD.MM.YYYY.".format(
                                 self.years_index[year]))
                date_check = False

            # Проверяем, чтобы год/месяц/день в поле1 не был больше года/месяца/дня в поле2
            else:
                day1, month1, year1 = self.date_fields[year * 2].get().split('.')
                day2, month2, year2 = self.date_fields[year * 2 + 1].get().split('.')

                if year1 == year2:
                    if month1 == month2:
                        if day1 > day2:
                            date_check = False
                    elif month1 > month2:
                        date_check = False
                elif year1 > year2:
                    date_check = False
                if (not date_check):
                    mb.showerror(title="Неверная дата!",
                                 message="Год неверен! Проверьте поля дат за {0} год.\nДата начала периода больше даты конца периода.".format(
                                     self.years_index[year]))
        return date_check

    def update_observer(self):
        '''Just for clarity, not really needed.'''
        pass

    ## Обработчик кнопки открытия браузера
    def open_in_browser(self, year):
        self._accumulate_data()

        if self._dates_check(_year=year):
            if self.court.current() != 0 and self.case_type.current() != 0:
                self.presenter.open_browser(year)
            else:
                self._update_main_text("Выберите вид судопроизводства и суд.\n")

    ## Обработчик кнопки "Открыть папку"
    def open_folder(self, event):
        if not self.presenter.folder_option(and_open=True):
            mb.showerror(title="Ошибка сохранения", message="У Вас нет прав записи данных в каталог отчетов.")

    # Обработчик кнопки "О программе"
    def about(self, event):
        mb.showinfo(title="О программе",
                    message="Автор: Сибирятко А.А.\nСеверо-Кавказский окружной военный суд.\nВерсия: 0.9.6")

    ## Обработчик кнопки "Выход"
    def quit_h(self, event):
        if self.if_calendar_exist:
            self.cal.forget()
            self.cal.destroy()
        self.destroy()


if __name__ == '__main__':
    root = Tk()
    root.withdraw()
    model = Model()
    pres = Presenter(model)
    main_app = MainWindow(pres, root)
    pres.set_view(main_app)
    root.mainloop()
