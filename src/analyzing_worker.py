from src.analyzing import make_url, fetch_parallel, _calc_cases_depending_on_case_type, _xlsx_export
from src.worker2 import Worker

__author__ = 'Various'


class AnalyzingWorker(Worker):

    def __init__(self):
        super().__init__()

    def work(self, data):
        # court_type, courts, first_date, second_date, year, case_type, judge
        """
        Функция проверки сайтов судов на наличие дел/актов.
        Принимает: Courts = список адресов судов, url = список url-адресов, Year = год анализа
        Возвращает: outCourtsData = список (int), где каждая позиция соответствует
        обработанному HTTP-запросу в url-списке
        """
        out_courts_data = []
        court_type = data['court_type']
        courts = data['courts']
        first_date = data['first_date']
        second_date = data['second_date']
        year = data['year']
        case_type = data['case_type']
        judge = data['judge']

        offset = 0  # переменная для создания отступа в файле xlsx

        url = make_url(first_date, second_date, court_type, judge)
        for next_court in range(len(courts)):
            if self.event.is_set():
                break
            else:
                final_url = []
                number = []

                for l in case_type.calc_range():
                    final_url.append(courts[next_court] + url[l])

                list_http_response = fetch_parallel(final_url)

                for k in range(len(list_http_response)):
                    if not self.event.is_set():
                        lines = []
                        is_string_found = True
                        found_number = 0

                        if not self.event.is_set():
                            try:
                                lines = list_http_response[k].readlines()
                            except AttributeError:
                                found_number = 0
                                number.append(found_number)
                                self.event.set()
                                # mb.showerror(title="Сообщение",
                                #              message="Произошла ошибка проверки данных. Перезапустите программу или обратитесь к разработчику.")
                                break

                        i = 0
                        # по умолчанию (i<len(lines)), maximumLine = 655, maxtotal_position = 253, minimumtotal_position = 160 ## and (i < 400)
                        while (i < len(lines)) and is_string_found:
                            line = lines[i]

                            total_position = line.find(b'\xe7\xe0\xef\xf0\xee\xf1\xf3 \xed\xe0\xe9\xe4\xe5\xed\xee')
                            if total_position > 0:
                                is_string_found = not is_string_found
                                total_position += 18
                                # Переводим из ascii в str, потом в int (ужас какой!)
                                while (line[total_position] >= 48) and (line[total_position] <= 57):
                                    found_number = str(found_number) + str(line[total_position] - 48)
                                    total_position += 1
                            i += 1
                        if is_string_found:
                            found_number = 0

                        found_number = int(found_number)
                        number.append(found_number)

                if not self.event.is_set():
                    calculated_data = _calc_cases_depending_on_case_type(number, case_type)
                    out_courts_data.append(calculated_data[0])
                    offset = calculated_data[1]

        # Проверяем доступ к папке и сохраняем файл
        if not self.event.is_set():
            try:
                _xlsx_export(out_courts_data, year, court_type, offset, courts, judge)
            except IOError:
                pass
            #     mb.showerror(title="Ошибка сохранения",
            #                  message="Открыт файл отчета, либо у Вас нет прав записи данных в каталог отчетов.")
        # else:
        #     if mb.askyesno(title="Сохранение данных",
        #                    message="Сохранить уже проверенные суды?"):
        #         try:
        #             _xlsx_export(out_courts_data, year, court_type, offset, courts[:-1], judge)
        #         except IOError:
        #             mb.showerror(title="Ошибка сохранения",
        #                          message="Открыт файл отчета, либо у Вас нет прав записи данных в каталог отчетов.")