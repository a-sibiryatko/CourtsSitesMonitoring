import os
import tkinter.ttk as ttk
import tkinter.messagebox as mb
import tkinter.filedialog as filedialog
from tkinter import *
from src.internal_types import Court, Date, GlobalStop


## Функция загрузки информации из .ini файла
def load_data():
    # Получение пути к файлу
    def get_file():
        root = Tk()
        root.title("Загрузка конф. файла")
        root.geometry("350x100+400+100")
        root.resizable(width=FALSE, height=FALSE)
   
        main_frame = Frame(root, bg="snow2")

        entry_text = StringVar()
        entry = Entry(main_frame, font=("times", 14), textvariable=entry_text)

        file_button = ttk.Button(main_frame, text="Открыть",  command=lambda: open_file(entry_text))
        ok_button = ttk.Button(main_frame, text="OK", command=lambda: return_result(entry_text))
        quit_button = ttk.Button(main_frame, text="Закрыть", command=lambda: quit_(root, entry_text))

        main_frame.place(width=350, height=100)
        entry.place(x=20, y=10, width=200)
        file_button.place(x=225, y=10)
        ok_button.place(x=110, y=60)
        quit_button.place(x=225, y=60)

        def open_file(entry):
            file_opt = options = {}
            options['defaultextension'] = '.ini'
            options['filetypes'] = [('all files', '.*'), ('ini files', '.ini')]
            options['initialdir'] = os.getcwd()
            options['initialfile'] = 'courts.ini'
            options['title'] = 'Расположение файла courts.ini'

            file_path = filedialog.askopenfilename(**file_opt)
            entry_text.set(file_path)

        def return_result(entry_text):
            root.destroy()

        def quit_(root, entry_text):
            entry_text.set("")
            root.destroy()

        root.mainloop()
        return entry_text.get()

    try:
        f = open(os.getcwd() + r"\courts.ini", 'r')
        # f = open("..courts.ini", 'r')
    except IOError as e:
        try:
            f = open(get_file())
        except IOError:
            raise SystemExit

    court_list = []
    current_court_type = ""
    court_types_list = []

    cur_year = ""
    years_list = []
    dates_list = []
    first_date = ""
    second_date = ""
    
    HeadersList = ["ТипСуда", "Год"]
    Lines = f.readlines()
    for line in Lines:
        if line[0] not in ('#', '\n', None):    # pass empty strings and startwith '#' strings
            Full = line.replace(" ", "").replace("\n", "").split("=")
            # Load courts inform
            if Full[0] == "ТипСуда" and Full[-1] not in court_types_list:
                current_court_type = Full[-1]
                court_types_list.append(Full[-1])

            elif Full[0] not in HeadersList and current_court_type:
                court_list.append(Court(current_court_type, Full[0], Full[-1]))

            # Load dates inform
            elif Full[0] == "Год" and Full[-1] not in years_list and len(years_list) <= 3:
                if first_date and second_date:
                    dates_list.append(Date(cur_year, first_date, second_date))
                    cur_year, first_date, second_date = "", "", ""
                cur_year = Full[-1]
                current_court_type = ""
                years_list.append(Full[-1])

            elif first_date and second_date and len(years_list) <= 3:
                dates_list.append(Date(cur_year, first_date, second_date))
                cur_year, first_date, second_date = "", "", ""

            elif Full[0] == "ДатаНачалаПериода" and cur_year:
                first_date = Full[-1]

            elif Full[0] == "ДатаКонцаПериода" and cur_year:
                second_date = Full[-1]
    f.close()
    
    if not court_list or len(dates_list) < 3:
        mb.showerror(title="Ошибка загрузки",
                     message="Заполните файл courts.ini соответственно инструкции внутри файла. \
                     Количество периодов(годов) должно быть равно трём.")
        sys.exit()
       # raise SystemExit
    else:
        return court_list, dates_list

if __name__ == "__main__":
    init_data = load_data()
    print(init_data[0])
    print(init_data[1])
