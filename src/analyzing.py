import threading
import urllib
import urllib.request
import urllib.error
import os
import re
import time
import binascii
import tkinter.messagebox as mb
from src.internal_types import GlobalStop

import xlsxwriter.workbook



# ##################################################################################################
from src.internal_types import CourtType, CaseTypeL1


def load_judge_list(court_type, case_type_code, court):
    judge_list = []
    try:
        if case_type_code:
            url = _init_judge_list_loader(case_type_code - 1, court)
            judge_list = _analyze_judge_list(url)
        else:
            if court_type == 0:
                url_g1_case = _init_judge_list_loader(0, court)
                url_u1_case = _init_judge_list_loader(1, court)
                url_adm1_case = _init_judge_list_loader(2, court)

                g1_judges = _analyze_judge_list(url_g1_case)
                u1_judges = _analyze_judge_list(url_u1_case)
                adm1_judges = _analyze_judge_list(url_adm1_case)

                judge_list = g1_judges + list(filter(lambda x: x not in g1_judges, u1_judges))
                judge_list = judge_list + list(filter(lambda x: x not in judge_list, adm1_judges))

            else:
                judge_list = []

        judge_list = _reform_judge_list(judge_list)
        return judge_list
    except (urllib.error.URLError, urllib.error.HTTPError):
        mb.showerror(title="Ошибка загрузки данных",
                     message="Не удалось загрузить данные. Возможно суд недоступен или Ваше соединение с Интернет прервано.")
        return []


def _init_judge_list_loader(case_type_code, court):
    case_type_codes_level = ["G1_CASE", "U1_CASE", "ADM_CASE", "", "", ""]

    judge_load_pattern = "/modules.php?name=sud_delo&srv_num=1&name_op=cat&nc=1&curent_delo_table=" + \
                         case_type_codes_level[case_type_code] + "&fieldname=JUDGE&h=450"
    url = court.get_court_address() + judge_load_pattern

    return url


def _analyze_judge_list(url):
    judge_list = []

    for line in urllib.request.urlopen(url, timeout=15).readlines():
        temp_line = ""

        for char in line.decode("cp1251"):
            if (ord(char) >= 1040 and ord(char) <= 1130) or (ord(char) == 46):
                temp_line += char

        if (temp_line):
            judge_list.append(temp_line)

    return judge_list


def _reform_judge_list(judge_list):
    judge_list = list(map(lambda x: x[: len(x) // 2], judge_list[1:]))  # отсекаем первое слово "Отмена"
    judge_list = _insert_whitespaces(judge_list)

    return judge_list


def _insert_whitespaces(judge_list):
    new_judge_list = []

    for judge_name in judge_list:
        if judge_name.endswith('.'):
            new_judge_list.append(judge_name[: len(judge_name) - 4] + ' ' + judge_name[len(judge_name) - 4:])
        else:
            new_judge_list.append(' '.join(re.findall('[А-Я][а-я]*', judge_name)))

    return new_judge_list


###################################################################################################
def checkSites(court_type, courts, first_date, second_date, year, case_type, judge):
    """
    Функция проверки сайтов судов на наличие дел/актов.
    Принимает: Courts = список адресов судов, url = список url-адресов, Year = год анализа
    Возвращает: outCourtsData = список (int), где каждая позиция соответствует
    обработанному HTTP-запросу в url-списке
    """
    out_courts_data = []
    offset = 0  # переменная для создания отступа в файле xlsx

    url = make_url(first_date, second_date, court_type, judge)
    for next_court in range(len(courts)):
        if not GlobalStop.sign:
            final_url = []
            number = []

            # main_window.update_main_text(
            #     "Проверяем {} суд за {} год.\nВыбранный судья - {}.\n".format(courts[next_court].get_court_name(), year, judge))

            for l in case_type.calc_range():
                final_url.append(courts[next_court] + url[l])

            list_http_response = fetch_parallel(final_url)

            for k in range(len(list_http_response)):
                if not GlobalStop.sign:
                    lines = []
                    is_string_found = True
                    found_number = 0

                    if not GlobalStop.sign:
                        try:
                            lines = list_http_response[k].readlines()
                        except AttributeError:
                            found_number = 0
                            number.append(found_number)
                            GlobalStop.sign = True
                            mb.showerror(title="Сообщение",
                                         message="Произошла ошибка проверки данных. Перезапустите программу или обратитесь к разработчику.")
                            break

                    i = 0
                    # по умолчанию (i<len(lines)), maximumLine = 655, maxtotal_position = 253, minimumtotal_position = 160 ## and (i < 400)
                    while (i < len(lines)) and is_string_found:
                        # update_counter += 1
                        # if update_counter % 10 == 0:
                        #     main_window.update()
                        line = lines[i]

                        total_position = line.find(b'\xe7\xe0\xef\xf0\xee\xf1\xf3 \xed\xe0\xe9\xe4\xe5\xed\xee')
                        if total_position > 0:
                            is_string_found = not is_string_found
                            total_position += 18
                            # Переводим из ascii в str, потом в int (ужас какой!)
                            while (line[total_position] >= 48) and (line[total_position] <= 57):
                                found_number = str(found_number) + str(line[total_position] - 48)
                                total_position += 1
                        i += 1
                    if is_string_found:
                        found_number = 0

                    found_number = int(found_number)
                    number.append(found_number)

            calculated_data = _calc_cases_depending_on_case_type(number, case_type)
            out_courts_data.append(calculated_data[0])
            offset = calculated_data[1]

    # Проверяем доступ к папке и сохраняем файл
    if not GlobalStop.sign:
        try:
            _xlsx_export(out_courts_data, year, court_type, offset, courts, judge)
        except IOError:
            mb.showerror(title="Ошибка сохранения",
                         message="Открыт файл отчета, либо у Вас нет прав записи данных в каталог отчетов.")
    # else:
    #     if mb.askyesno(title="Сохранение данных",
    #                    message="Сохранить уже проверенные суды?"):
    #         try:
    #             _xlsx_export(out_courts_data, year, court_type, offset, courts[:-1], judge)
    #         except IOError:
    #             mb.showerror(title="Ошибка сохранения",
    #                          message="Открыт файл отчета, либо у Вас нет прав записи данных в каталог отчетов.")


###################################################################################################
def make_url(first_date, second_date, court_type, judge=None):
    '''
    Функция для создания списка URL для анализа GVS и SKOVS.
    Принимает: firstDate=дата начала периода, secondDate=дата конца периода
    CourtType=["GVS", "SKOVS"]=тип суда для анализа, Year=год анализа для выбора значений по умолчанию
    Возвращает: baseUrlGVS  с вставленными firstDate и secondDate
    '''
    _judge = re.sub("(..)", "%\\1", str(binascii.hexlify(judge.encode('cp1251')), 'cp1251')).replace('%',
                                                                                                     '%25') if judge and judge != 'Все судьи' else ''

    ## Базовый список адресов для анализа гарнизонных судов. Полный список с датами анализа формируется функцией make_url
    ## Конечная структура - URL = [Гр.дела, Гр.акты, Гр.возвр.дела, Гр.возвр.акты, Гр.отказ.дела, Гр.отказ.акты, Гр.прекр.дела, Гр.прекр.акты, Гр.перед.дела, Гр.перед.акты, Уг.дела, Уг.акты]
    _base_url_level1 = [
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1540005&case_type=&new=&G1_PARTS__NAMESS=&g1_case__CASE_NUMBERSS=&delo_table=g1_case&g1_case__ENTRY_DATE1D=&g1_case__ENTRY_DATE2D=&g1_case__ORIGIN_DATE1D=&g1_case__ORIGIN_DATE2D=&G1_CASE__JUDGE=" + _judge + "&g1_case__RESULT_DATE1D=",
        "&g1_case__RESULT_DATE2D=",
        "&G1_CASE__RESULT=&g1_case__VALIDITY_DATE1D=&g1_case__VALIDITY_DATE2D=&G1_EVENT__EVENT_NAME=&G1_EVENT__EVENT_DATE1D=&G1_EVENT__EVENT_DATE2D=&G1_PARTS__PARTS_TYPE=&G1_DOCUMENT__PUBL_DATE1D=&G1_DOCUMENT__PUBL_DATE2D=&G1_CASE__VALIDITY_DATE1D=&G1_CASE__VALIDITY_DATE2D=&Submit=%CD%E0%E9%F2%E8",
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1540005&case_type=&new=&G1_PARTS__NAMESS=&g1_case__CASE_NUMBERSS=&delo_table=g1_case&g1_case__ENTRY_DATE1D=&g1_case__ENTRY_DATE2D=&g1_case__ORIGIN_DATE1D=&g1_case__ORIGIN_DATE2D=&G1_CASE__JUDGE=" + _judge + "&g1_case__RESULT_DATE1D=",
        "&g1_case__RESULT_DATE2D=",
        "&G1_CASE__RESULT=&g1_case__VALIDITY_DATE1D=&g1_case__VALIDITY_DATE2D=&G1_EVENT__EVENT_NAME=&G1_EVENT__EVENT_DATE1D=&G1_EVENT__EVENT_DATE2D=&G1_PARTS__PARTS_TYPE=&G1_DOCUMENT__LOAD_DATE1D=",
        "&G1_DOCUMENT__LOAD_DATE2D=&G1_CASE__VALIDITY_DATE1D=&G1_CASE__VALIDITY_DATE2D=&Submit=%CD%E0%E9%F2%E8#",
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1540005&case_type=&new=&G1_PARTS__NAMESS=&g1_case__CASE_NUMBERSS=&delo_table=g1_case&g1_case__ENTRY_DATE1D=&g1_case__ENTRY_DATE2D=&g1_case__ORIGIN_DATE1D=&g1_case__ORIGIN_DATE2D=&G1_CASE__JUDGE=" + _judge + "&g1_case__RESULT_DATE1D=",
        "&g1_case__RESULT_DATE2D=",
        "&G1_CASE__RESULT=%25C7%25E0%25FF%25E2%25EB%25E5%25ED%25E8%25E5%2520%25C2%25CE%25C7%25C2%25D0%25C0%25D9%25C5%25CD%25CE%2520%25E7%25E0%25FF%25E2%25E8%25F2%25E5%25EB%25FE&g1_case__VALIDITY_DATE1D=&g1_case__VALIDITY_DATE2D=&G1_EVENT__EVENT_NAME=&G1_EVENT__EVENT_DATE1D=&G1_EVENT__EVENT_DATE2D=&G1_PARTS__PARTS_TYPE=&G1_DOCUMENT__LOAD_DATE1D=&G1_DOCUMENT__LOAD_DATE2D=&G1_CASE__VALIDITY_DATE1D=&G1_CASE__VALIDITY_DATE2D=&Submit=%CD%E0%E9%F2%E8",
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1540005&case_type=&new=&G1_PARTS__NAMESS=&g1_case__CASE_NUMBERSS=&delo_table=g1_case&g1_case__ENTRY_DATE1D=&g1_case__ENTRY_DATE2D=&g1_case__ORIGIN_DATE1D=&g1_case__ORIGIN_DATE2D=&G1_CASE__JUDGE=" + _judge + "&g1_case__RESULT_DATE1D=",
        "&g1_case__RESULT_DATE2D=",
        "&G1_CASE__RESULT=%25C7%25E0%25FF%25E2%25EB%25E5%25ED%25E8%25E5%2520%25C2%25CE%25C7%25C2%25D0%25C0%25D9%25C5%25CD%25CE%2520%25E7%25E0%25FF%25E2%25E8%25F2%25E5%25EB%25FE&g1_case__VALIDITY_DATE1D=&g1_case__VALIDITY_DATE2D=&G1_EVENT__EVENT_NAME=&G1_EVENT__EVENT_DATE1D=&G1_EVENT__EVENT_DATE2D=&G1_PARTS__PARTS_TYPE=&G1_DOCUMENT__LOAD_DATE1D=",
        "&G1_DOCUMENT__LOAD_DATE2D=&G1_CASE__VALIDITY_DATE1D=&G1_CASE__VALIDITY_DATE2D=&Submit=%CD%E0%E9%F2%E8",
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1540005&case_type=&new=&G1_PARTS__NAMESS=&g1_case__CASE_NUMBERSS=&delo_table=g1_case&g1_case__ENTRY_DATE1D=&g1_case__ENTRY_DATE2D=&g1_case__ORIGIN_DATE1D=&g1_case__ORIGIN_DATE2D=&G1_CASE__JUDGE=" + _judge + "&g1_case__RESULT_DATE1D=",
        "&g1_case__RESULT_DATE2D=",
        "&G1_CASE__RESULT=%25CE%25D2%25CA%25C0%25C7%25C0%25CD%25CE%2520%25E2%2520%25EF%25F0%25E8%25ED%25FF%25F2%25E8%25E8%2520%25E7%25E0%25FF%25E2%25EB%25E5%25ED%25E8%25FF&g1_case__VALIDITY_DATE1D=&g1_case__VALIDITY_DATE2D=&G1_EVENT__EVENT_NAME=&G1_EVENT__EVENT_DATE1D=&G1_EVENT__EVENT_DATE2D=&G1_PARTS__PARTS_TYPE=&G1_DOCUMENT__LOAD_DATE1D=&G1_DOCUMENT__LOAD_DATE2D=&G1_CASE__VALIDITY_DATE1D=&G1_CASE__VALIDITY_DATE2D=&Submit=%CD%E0%E9%F2%E8",
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1540005&case_type=&new=&G1_PARTS__NAMESS=&g1_case__CASE_NUMBERSS=&delo_table=g1_case&g1_case__ENTRY_DATE1D=&g1_case__ENTRY_DATE2D=&g1_case__ORIGIN_DATE1D=&g1_case__ORIGIN_DATE2D=&G1_CASE__JUDGE=" + _judge + "&g1_case__RESULT_DATE1D=",
        "&g1_case__RESULT_DATE2D=",
        "&G1_CASE__RESULT=%25CE%25D2%25CA%25C0%25C7%25C0%25CD%25CE%2520%25E2%2520%25EF%25F0%25E8%25ED%25FF%25F2%25E8%25E8%2520%25E7%25E0%25FF%25E2%25EB%25E5%25ED%25E8%25FF&g1_case__VALIDITY_DATE1D=&g1_case__VALIDITY_DATE2D=&G1_EVENT__EVENT_NAME=&G1_EVENT__EVENT_DATE1D=&G1_EVENT__EVENT_DATE2D=&G1_PARTS__PARTS_TYPE=&G1_DOCUMENT__LOAD_DATE1D=",
        "&G1_DOCUMENT__LOAD_DATE2D=&G1_CASE__VALIDITY_DATE1D=&G1_CASE__VALIDITY_DATE2D=&Submit=%CD%E0%E9%F2%E8",
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1540006&case_type=&new=&U1_DEFENDANT__NAMESS=&u1_case__CASE_NUMBERSS=&delo_table=u1_case&u1_case__ENTRY_DATE1D=&u1_case__ENTRY_DATE2D=&U1_CASE__JUDGE=" + _judge + "&u1_case__RESULT_DATE1D=",
        "&u1_case__RESULT_DATE2D=",
        "&U1_CASE__RESULT=&u1_case__VALIDITY_DATE1D=&u1_case__VALIDITY_DATE2D=&U1_EVENT__EVENT_NAME=&U1_EVENT__EVENT_DATE1D=&U1_EVENT__EVENT_DATE2D=&U1_DEFENDANT__VERDICT_DATE1D=&U1_DEFENDANT__VERDICT_DATE2D=&U1_DEFENDANT__RESULT=&U1_DEFENDANT__LAW_ARTICLE=&U1_PARTS__PARTS_TYPE=&U1_CASE__VALIDITY_DATE1D=&U1_CASE__VALIDITY_DATE2D=&U1_DOCUMENT__LOAD_DATE1D=&U1_DOCUMENT__LOAD_DATE2D=&Submit=%CD%E0%E9%F2%E8",
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1540006&case_type=&new=&U1_DEFENDANT__NAMESS=&u1_case__CASE_NUMBERSS=&delo_table=u1_case&u1_case__ENTRY_DATE1D=&u1_case__ENTRY_DATE2D=&U1_CASE__JUDGE=" + _judge + "&u1_case__RESULT_DATE1D=",
        "&u1_case__RESULT_DATE2D=",
        "&U1_CASE__RESULT=&u1_case__VALIDITY_DATE1D=&u1_case__VALIDITY_DATE2D=&U1_EVENT__EVENT_NAME=&U1_EVENT__EVENT_DATE1D=&U1_EVENT__EVENT_DATE2D=&U1_DEFENDANT__VERDICT_DATE1D=&U1_DEFENDANT__VERDICT_DATE2D=&U1_DEFENDANT__RESULT=&U1_DEFENDANT__LAW_ARTICLE=&U1_PARTS__PARTS_TYPE=&U1_CASE__VALIDITY_DATE1D=&U1_CASE__VALIDITY_DATE2D=&U1_DOCUMENT__LOAD_DATE1D=",
        "&U1_DOCUMENT__LOAD_DATE2D=&Submit=%CD%E0%E9%F2%E8",
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1500001&case_type=&new=&adm_parts__NAMESS=&adm_case__CASE_NUMBERSS=&delo_table=adm_case&adm_case__ENTRY_DATE1D=&adm_case__ENTRY_DATE2D=&ADM_CASE__JUDGE=" + _judge + "&adm_case__RESULT_DATE1D=",
        "&adm_case__RESULT_DATE2D=",
        "&ADM_CASE__RESULT=&ADM_CASE__SHORT_NUMBER=&adm_case__VALIDITY_DATE1D=&adm_case__VALIDITY_DATE2D=&ADM_EVENT__EVENT_NAME=&adm_event__EVENT_DATE1D=&adm_event__EVENT_DATE2D=&ADM_PARTS__LAW_ARTICLE=&adm_document__LOAD_DATE1D=&adm_document__LOAD_DATE2D=&ADM_CASE__VALIDITY_DATE1D=&ADM_CASE__VALIDITY_DATE2D=&Submit=%CD%E0%E9%F2%E8",
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1500001&case_type=&new=&adm_parts__NAMESS=&adm_case__CASE_NUMBERSS=&delo_table=adm_case&adm_case__ENTRY_DATE1D=&adm_case__ENTRY_DATE2D=&ADM_CASE__JUDGE=" + _judge + "&adm_case__RESULT_DATE1D=",
        "&adm_case__RESULT_DATE2D=",
        "&ADM_CASE__RESULT=&ADM_CASE__SHORT_NUMBER=&adm_case__VALIDITY_DATE1D=&adm_case__VALIDITY_DATE2D=&ADM_EVENT__EVENT_NAME=&adm_event__EVENT_DATE1D=&adm_event__EVENT_DATE2D=&ADM_PARTS__LAW_ARTICLE=&adm_document__LOAD_DATE1D=",
        "&adm_document__LOAD_DATE2D=&ADM_CASE__VALIDITY_DATE1D=&ADM_CASE__VALIDITY_DATE2D=&Submit=%CD%E0%E9%F2%E8",
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1500001&case_type=&new=&adm_parts__NAMESS=&adm_case__CASE_NUMBERSS=&delo_table=adm_case&adm_case__ENTRY_DATE1D=&adm_case__ENTRY_DATE2D=&ADM_CASE__JUDGE=" + _judge + "&adm_case__RESULT_DATE1D=",
        "&adm_case__RESULT_DATE2D=",
        "&ADM_CASE__RESULT=%25C2%25FB%25ED%25E5%25F1%25E5%25ED%25EE%2520%25EE%25EF%25F0%25E5%25E4%25E5%25EB%25E5%25ED%25E8%25E5%2520%25EE%2520%25E2%25EE%25E7%25E2%25F0%25E0%25F9%25E5%25ED%25E8%25E8%2520%25EF%25F0%25EE%25F2%25EE%25EA%25EE%25EB%25E0%2520%25EE%25E1%2520%25C0%25CF%2520%25E8%2520%25E4%25F0.%2520%25EC%25E0%25F2%25E5%25F0%25E8%25E0%25EB%25EE%25E2%2520%25E4%25E5%25EB%25E0%2520%25E2%2520%25EE%25F0%25E3%25E0%25ED%252C%2520%25E4%25EE%25EB%25E6.%2520%25EB%25E8%25F6%25F3%2520...%2520%25E2%2520%25F1%25EB%25F3%25F7%25E0%25E5%2520%25F1%25EE%25F1%25F2%25E0%25E2%25EB%25E5%25ED%25E8%25FF%2520%25EF%25F0%25EE%25F2%25EE%25EA%25EE%25EB%25E0%2520%25E8%2520%25EE%25F4%25EE%25F0%25EC%25EB%25E5%25ED%25E8%25FF%2520%25E4%25F0%25F3%25E3%25E8%25F5%2520%25EC%25E0%25F2%25E5%25F0%25E8%25E0%25EB%25EE%25E2%2520%25E4%25E5%25EB%25E0%2520%25ED%25E5%25EF%25F0%25E0%25E2%25EE%25EC%25EE%25F7%25ED%25FB%25EC%25E8%2520%25EB%25E8%25F6%25E0%25EC%25E8%252C%2520...&ADM_CASE__SHORT_NUMBER=&adm_case__VALIDITY_DATE1D=&adm_case__VALIDITY_DATE2D=&ADM_EVENT__EVENT_NAME=&adm_event__EVENT_DATE1D=&adm_event__EVENT_DATE2D=&ADM_PARTS__LAW_ARTICLE=&adm_document__LOAD_DATE1D=&adm_document__LOAD_DATE2D=&ADM_CASE__VALIDITY_DATE1D=&ADM_CASE__VALIDITY_DATE2D=&Submit=%CD%E0%E9%F2%E8",
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1500001&case_type=&new=&adm_parts__NAMESS=&adm_case__CASE_NUMBERSS=&delo_table=adm_case&adm_case__ENTRY_DATE1D=&adm_case__ENTRY_DATE2D=&ADM_CASE__JUDGE=" + _judge + "&adm_case__RESULT_DATE1D=",
        "&adm_case__RESULT_DATE2D=",
        "&ADM_CASE__RESULT=%25C2%25FB%25ED%25E5%25F1%25E5%25ED%25EE%2520%25EE%25EF%25F0%25E5%25E4%25E5%25EB%25E5%25ED%25E8%25E5%2520%25EE%2520%25E2%25EE%25E7%25E2%25F0%25E0%25F9%25E5%25ED%25E8%25E8%2520%25EF%25F0%25EE%25F2%25EE%25EA%25EE%25EB%25E0%2520%25EE%25E1%2520%25C0%25CF%2520%25E8%2520%25E4%25F0.%2520%25EC%25E0%25F2%25E5%25F0%25E8%25E0%25EB%25EE%25E2%2520%25E4%25E5%25EB%25E0%2520%25E2%2520%25EE%25F0%25E3%25E0%25ED%252C%2520%25E4%25EE%25EB%25E6.%2520%25EB%25E8%25F6%25F3%2520...%2520%25E2%2520%25F1%25EB%25F3%25F7%25E0%25E5%2520%25F1%25EE%25F1%25F2%25E0%25E2%25EB%25E5%25ED%25E8%25FF%2520%25EF%25F0%25EE%25F2%25EE%25EA%25EE%25EB%25E0%2520%25E8%2520%25EE%25F4%25EE%25F0%25EC%25EB%25E5%25ED%25E8%25FF%2520%25E4%25F0%25F3%25E3%25E8%25F5%2520%25EC%25E0%25F2%25E5%25F0%25E8%25E0%25EB%25EE%25E2%2520%25E4%25E5%25EB%25E0%2520%25ED%25E5%25EF%25F0%25E0%25E2%25EE%25EC%25EE%25F7%25ED%25FB%25EC%25E8%2520%25EB%25E8%25F6%25E0%25EC%25E8%252C%2520...&ADM_CASE__SHORT_NUMBER=&adm_case__VALIDITY_DATE1D=&adm_case__VALIDITY_DATE2D=&ADM_EVENT__EVENT_NAME=&adm_event__EVENT_DATE1D=&adm_event__EVENT_DATE2D=&ADM_PARTS__LAW_ARTICLE=&adm_document__LOAD_DATE1D=",
        "&adm_document__LOAD_DATE2D=&ADM_CASE__VALIDITY_DATE1D=&ADM_CASE__VALIDITY_DATE2D=&Submit=%CD%E0%E9%F2%E8",
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1500001&case_type=&new=&adm_parts__NAMESS=&adm_case__CASE_NUMBERSS=&delo_table=adm_case&adm_case__ENTRY_DATE1D=&adm_case__ENTRY_DATE2D=&ADM_CASE__JUDGE=" + _judge + "&adm_case__RESULT_DATE1D=",
        "&adm_case__RESULT_DATE2D=",
        "&ADM_CASE__RESULT=%25C2%25FB%25ED%25E5%25F1%25E5%25ED%25EE%2520%25EE%25EF%25F0%25E5%25E4%25E5%25EB%25E5%25ED%25E8%25E5%2520%25EE%2520%25EF%25E5%25F0%25E5%25E4%25E0%25F7%25E5%2520%25E4%25E5%25EB%25E0%2520%25EF%25EE%2520%25EF%25EE%25E4%25E2%25E5%25E4%25EE%25EC%25F1%25F2%25E2%25E5%25ED%25ED%25EE%25F1%25F2%25E8%2520%2528%25F1%25F2%252029.9%2520%25F7.2%2520%25EF.2%2520%25E8%2520%25F1%25F2%252029.4%2520%25F7.1%2520%25EF.5%2529&ADM_CASE__SHORT_NUMBER=&adm_case__VALIDITY_DATE1D=&adm_case__VALIDITY_DATE2D=&ADM_EVENT__EVENT_NAME=&adm_event__EVENT_DATE1D=&adm_event__EVENT_DATE2D=&ADM_PARTS__LAW_ARTICLE=&adm_document__LOAD_DATE1D=&adm_document__LOAD_DATE2D=&ADM_CASE__VALIDITY_DATE1D=&ADM_CASE__VALIDITY_DATE2D=&Submit=%CD%E0%E9%F2%E8",
        "/modules.php?name=sud_delo&srv_num=1&name_op=r&delo_id=1500001&case_type=&new=&adm_parts__NAMESS=&adm_case__CASE_NUMBERSS=&delo_table=adm_case&adm_case__ENTRY_DATE1D=&adm_case__ENTRY_DATE2D=&ADM_CASE__JUDGE=" + _judge + "&adm_case__RESULT_DATE1D=",
        "&adm_case__RESULT_DATE2D=",
        "&ADM_CASE__RESULT=%25C2%25FB%25ED%25E5%25F1%25E5%25ED%25EE%2520%25EE%25EF%25F0%25E5%25E4%25E5%25EB%25E5%25ED%25E8%25E5%2520%25EE%2520%25EF%25E5%25F0%25E5%25E4%25E0%25F7%25E5%2520%25E4%25E5%25EB%25E0%2520%25EF%25EE%2520%25EF%25EE%25E4%25E2%25E5%25E4%25EE%25EC%25F1%25F2%25E2%25E5%25ED%25ED%25EE%25F1%25F2%25E8%2520%2528%25F1%25F2%252029.9%2520%25F7.2%2520%25EF.2%2520%25E8%2520%25F1%25F2%252029.4%2520%25F7.1%2520%25EF.5%2529&ADM_CASE__SHORT_NUMBER=&adm_case__VALIDITY_DATE1D=&adm_case__VALIDITY_DATE2D=&ADM_EVENT__EVENT_NAME=&adm_event__EVENT_DATE1D=&adm_event__EVENT_DATE2D=&ADM_PARTS__LAW_ARTICLE=&adm_document__LOAD_DATE1D=",
        "&adm_document__LOAD_DATE2D=&ADM_CASE__VALIDITY_DATE1D=&ADM_CASE__VALIDITY_DATE2D=&Submit=%CD%E0%E9%F2%E8"]  # G1_DOCUMENT__PUBL_DATE1D


    ## Базовый список адресов для анализа окружного суда. Полный список с датами анализа формируется функцией make_url.
    ## Конечная структура
    ## URL = [Гр1.дела, Гр1.акты, Гр1.возвр.дела, Гр1.возвр.акты, Гр1.отказ.дела, Гр1.отказ.акты, Уг1.дела, Уг1.акты, ДСП1.дела, ДСП1.акты, Гр.апел.дела, Гр.апел.акты, Уг.апел.дела, Уг.апел.акты, Адм1.дела, Адм1.акты,
    ##        Гр.надзор.дела, Гр.надзор.акты, Уг.надзор.дела, Уг.надзор.акты, Гр.жалобы.дела, Гр.жалобы.акты, Уг.жалобы.дела, Уг.жалобы.акты, Адм.жалобы.дела, Адм.жалобы.акты]
    _base_url_level2 = []

    base_url_pos = 0  # позиция для обхода списка
    base_length = 0  # конечная длина списка, исключая разрывы дат
    final_url_list = []  # итоговый список
    base_list = []

    if court_type == CourtType.LevelOne:
        base_list = _base_url_level1
        base_length = 14

    elif court_type == CourtType.LevelTwo:
        base_list = _base_url_level2
        base_length = 16

    for i in range(base_length):
        if i % 2 == 0:
            final_url_list.append(
                base_list[base_url_pos] + first_date + base_list[base_url_pos + 1] + second_date + base_list[
                    base_url_pos + 2])
            base_url_pos += 3

        else:
            final_url_list.append(
                base_list[base_url_pos] + first_date + base_list[base_url_pos + 1] + second_date + base_list[
                    base_url_pos + 2] + first_date + base_list[base_url_pos + 3])
            base_url_pos += 4

    return final_url_list


###################################################################################################
def fetch_parallel(urls):
    '''
    Функция многопотокового создания url-запросов
    Принимает: url = список адресов
    Возвращает: result = список HTTP-загрузок
    '''
    result = [0] * len(urls)
    threads = [threading.Thread(target=read_url, args=(url, result, _pos)) for _pos, url in enumerate(urls)]
    for t in threads:
        t.start()
        time.sleep(0.5)

    return result


def read_url(url, f_list, pos):
    '''
    Функция чтения url
    Принимает: url = адрес, f_list = итоговый список HTTP-загрузок,
    pos = позиция обработанного url запроса в f_list
    Возвращает: f_list с вставленной в него HTTP-ответом в позицию Pos
    '''
    data = 0
    try:
        data = urllib.request.urlopen(url)
    except urllib.error.HTTPError:
        return

    f_list[pos] = data
    return


###################################################################################################
def _calc_cases_depending_on_case_type(number, case_type):
    # Определяем, как будут считаться конечные сведения
    # в зависимости от принятого типа дело-ства
    Number = number
    offset = 0

    if isinstance(case_type, CaseTypeL1):
        if case_type == CaseTypeL1.ALL_TYPES:  #полная выборка
            Number[0] -= (Number[2] + Number[4])
            Number[1] -= (Number[3] + Number[5])
            Number[-6] -= (Number[-4] + Number[-2])
            Number[-5] -= (Number[-3] + Number[-1])
            offset = 0

        elif case_type == CaseTypeL1.ADM1_CASE:  # только административные
            Number[0] -= (Number[2] + Number[4])
            Number[1] -= (Number[3] + Number[5])
            offset = 8

        elif case_type == CaseTypeL1.U1_CASE:  # только уголовные
            offset = 6

        elif case_type == CaseTypeL1.G1_CASE:  # только гражданские
            Number[0] -= (Number[2] + Number[4])
            Number[1] -= (Number[3] + Number[5])
            offset = 0

    return Number, offset


###################################################################################################
def _xlsx_export(data, year, court_type, offset, courts, judge):
    '''
    Функция создания xlxs-файла с данными
    data = 2-мерный список результатов поиска,
    year (int) = год для создания разл файлов,
    court_type = [0, 1] = тип суда, определяющий процесс экспорта
    offset = отступ записи в строке, зависящий от вида судопроизводства
    courts = список проанализированных судов
    judge = все судьи либо один судья
    '''
    workbook = xlsxwriter.workbook.Workbook(
        os.environ.get("APPDATA") + "\\Мониторинг\\Мониторинг_{0}_{1}.xlsx".format(court_type.text(), year))
    worksheet = workbook.add_worksheet()

    _judge = ' - ' + judge if judge and judge != 'Все судьи' else ''

    # Формат для заголовка: 12 шрифт, Times New Roman, жирный, выравнивание по центру
    FormatTitle = workbook.add_format()
    FormatTitle.set_font_name('Times New Roman')
    FormatTitle.set_font_size(12)
    FormatTitle.set_border()
    FormatTitle.set_bold()
    FormatTitle.set_align('center')
    FormatTitle.set_align('vcenter')
    FormatTitle.set_text_wrap()
    # Формат обычного текста: 12 шрифт, Times New Roman, выравнивание по центру
    FormatText = workbook.add_format()
    FormatText.set_font_name('Times New Roman')
    FormatText.set_font_size(12)
    FormatText.set_border()
    FormatText.set_align('center')
    FormatText.set_align('vcenter')
    FormatText.set_text_wrap()
    # Формат цветного текста: 12 шрифт, Times New Roman, выравнивание по центру, зеленый
    FormatGreen = workbook.add_format()
    FormatGreen.set_font_name('Times New Roman')
    FormatGreen.set_font_size(12)
    FormatGreen.set_border()
    FormatGreen.set_bold()
    FormatGreen.set_align('center')
    FormatGreen.set_align('vcenter')
    FormatGreen.set_bg_color('green')
    # Записываем шапку таблицы
    if court_type == CourtType.LevelOne:
        worksheet.write(0, 0, "№ п/п", FormatTitle)
        worksheet.write(0, 1, "Наименование суда", FormatTitle)
        worksheet.write(0, 2, "Гр. дела", FormatGreen)
        worksheet.write(0, 3, "Гр. акты", FormatGreen)
        worksheet.write(0, 4, "Гр. возвр. дела", FormatTitle)
        worksheet.write(0, 5, "Гр. возвр. акты", FormatTitle)
        worksheet.write(0, 6, "Гр. отказ. дела", FormatTitle)
        worksheet.write(0, 7, "Гр. отказ. акты", FormatTitle)
        worksheet.write(0, 8, "Уг. дела", FormatGreen)
        worksheet.write(0, 9, "Уг. акты", FormatGreen)
        worksheet.write(0, 10, "Адм. дела", FormatGreen)
        worksheet.write(0, 11, "Адм. акты", FormatGreen)
        worksheet.write(0, 12, "Адм. возвр. дела", FormatTitle)
        worksheet.write(0, 13, "Адм. возвр. акты", FormatTitle)
        worksheet.write(0, 14, "Адм. перед. дела", FormatTitle)
        worksheet.write(0, 15, "Адм. перед. акты", FormatTitle)
        # Настройка ширины столбцов: A:7, B:33, C-H:15
        worksheet.set_column(0, 1, 7)
        worksheet.set_column(1, 2, 33)
        worksheet.set_column(2, 17, 15)
        worksheet.freeze_panes(1, 2)

    elif court_type == CourtType.LevelTwo:
        pass

    # Пишем в файл: {Наименование суда} | {столбцы аналогично собранному списку baseUrl}
    for rNumber in range(1, len(data) + 1):
        worksheet.write(rNumber, 0, rNumber, FormatText)
        worksheet.write(rNumber, 1, courts[rNumber - 1].get_court_name() + _judge, FormatText)
        for cNumber in range(1, len(data[rNumber - 1]) + 1):
            worksheet.write(rNumber, cNumber + 1 + offset, data[rNumber - 1][cNumber - 1], FormatText)
    workbook.close()
