from tkinter import Tk
from src.model import Model
from src.presenter import Presenter
from src.view import MainWindow

# # Основное окно программы
if __name__ == '__main__':
    root = Tk()
    root.withdraw()
    model = Model()
    pres = Presenter(model)
    main_app = MainWindow(pres, root)
    pres.set_view(main_app)
    root.mainloop()
